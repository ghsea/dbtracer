create database if not exists dbtracer  DEFAULT CHARACTER SET utf8;
use dbtracer;
create table db_history_log(
id  bigint auto_increment,
original_key bigint not null comment '业务数据key,一般是对应表的主键',
biz_type int not null comment '',
biz_name nvarchar(50) not null comment '',
chang_log nvarchar(500) not null comment '数据变更前后的对照',
operator nvarchar(20) comment '操作者姓名',
create_time datetime not null comment '创建时间',
primary key(id)
)ENGINE=InnoDB;