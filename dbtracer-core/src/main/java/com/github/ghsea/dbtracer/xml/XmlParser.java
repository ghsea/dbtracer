package com.github.ghsea.dbtracer.xml;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * 
 * @author: guhai
 * @date:2017年8月9日 下午9:15:56
 */
public class XmlParser {

    private final String Element_TABLE = "table";

    private File xmlFile;

    private String classPath = Class.class.getResource("/").getPath();

    private Logger logger = LoggerFactory.getLogger(XmlParser.class);

    public XmlParser(String xmlPath) {
        String file = classPath + xmlPath;
        xmlFile = new File(file);
    }

    public Map<String, TableConfiguration> parse() {
        Map<String, TableConfiguration> tblName2Config = new HashMap<String, TableConfiguration>();
        try {
            SAXReader reader = newSAXReader();
            Document doc = reader.read(xmlFile);

            Element root = doc.getRootElement();
            for (Iterator<Element> i = root.elementIterator(Element_TABLE); i.hasNext();) {
                TableConfiguration tblConfig = new TableConfiguration();
                Element tblElement = (Element) i.next();
                Attribute nameAttr = tblElement.attribute("name");
                String tableName = nameAttr.getValue();
                tblConfig.setName(tableName);

                Attribute keyAttr = tblElement.attribute("key");
                String keyName = keyAttr.getValue();
                tblConfig.setKey(keyName);

                Iterator<Element> filedElements = tblElement.elementIterator("field");
                while (filedElements.hasNext()) {
                    Element filedElement = filedElements.next();
                    TableField field = parseFiledElement(filedElement);
                    tblConfig.getFiledName2Config().put(field.getNameEn(), field);
                }

                // TODO 表名配置重复就抛异常
                tblName2Config.put(tableName, tblConfig);
                System.out.println(tblConfig);
            }
        } catch (DocumentException ex) {
            ex.printStackTrace();
        }

        return tblName2Config;
    }

    private SAXReader newSAXReader() {
        EntityResolver entityResolver = new EntityResolver() {
            public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
                String xsdPath = "/com/github/ghsea/dbtracer/xml/dbtracer.xsd";
                InputStream xsdIs = XmlParser.class.getResourceAsStream(xsdPath);
                if (xsdIs == null) {
                    throw new RuntimeException(new FileNotFoundException(xsdPath));
                }
                return new InputSource(xsdIs);
            }
        };

        // 设置校验
        SAXReader reader = new SAXReader(true);
        try {
            reader.setFeature("http://apache.org/xml/features/validation/schema", true);
            reader.setFeature("http://apache.org/xml/features/validation/schema-full-checking", true);
        } catch (SAXException e) {
            logger.error(e.getMessage(), e);
        }
        reader.setEntityResolver(entityResolver);
        return reader;
    }

    private TableField parseFiledElement(Element filedElement) {
        TableField filed = new TableField();
        String nameEN = filedElement.attribute("nameEN").getValue();
        filed.setNameEn(nameEN);
        String nameCN = filedElement.attribute("nameCN").getValue();
        filed.setNameCn(nameCN);

        Iterator<Element> propertyElements = filedElement.elementIterator("property");
        while (propertyElements.hasNext()) {
            Element propertyElement = propertyElements.next();
            String value = propertyElement.attribute("value").getValue();
            String description = propertyElement.attribute("description").getValue();
            filed.getValue2Description().put(value, description);
        }

        return filed;
    }
}
