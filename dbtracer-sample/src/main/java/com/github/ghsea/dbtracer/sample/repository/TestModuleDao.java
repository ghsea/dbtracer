package com.github.ghsea.dbtracer.sample.repository;

import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.annotation.MapperScan;

import com.github.ghsea.dbtracer.sample.module.TestModule;

@MapperScan
public interface TestModuleDao {

	void updateByPk(@Param("test") TestModule testModule);

}
